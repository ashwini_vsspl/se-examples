import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router'


@Component({
  selector: 'events',
  templateUrl: './example.component.html',
  styleUrls: ['./example.component.css']
})
export class ExampleComponent implements OnInit {
  showFiller:boolean= false;
  name:string="";
  password:string;
  clicked:boolean=false;

  events = [
    {name: "Overview",title:'Simple autocomplete', content: 'The autocomplete is a normal text input enhanced by a panel of suggested options', id: 1},  
    {name: "API", title:'API reference for Angular Material autocomplete',content:'import {MatAutocompleteModule} from @angular/material/autocomplete', id: 2},  
    {name: "images", title:'', id: 3},       
  ]
  currentevent: any;
  
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      this.currentevent = this.events.filter(m => m.id === +params['id'])[0];
    })
  }
  togglebtn(){
    this.showFiller=!this.showFiller;
  }
  sub(){
    this.clicked=!this.clicked;
    
  }
  
}




