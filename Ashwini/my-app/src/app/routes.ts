import { Routes } from '@angular/router'
import { ExampleComponent} from './example/example.component'

export const appRoutes:Routes=[
    { path: '',redirectTo: '/events/1',pathMatch: 'full'},
    { path: 'events/:id',component: ExampleComponent}
]