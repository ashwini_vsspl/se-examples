import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router';
import { appRoutes } from './routes'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule, 
  MatCheckboxModule,
  MatCardModule,
  MatRadioModule,
  MatSidenavModule,
  MatPaginatorModule,
  MatFormFieldModule,
  MatToolbarModule,
  MatIconModule,
  MatExpansionModule,

} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { ExampleComponent } from './example/example.component';
import { ContentComponent } from './content/content.component'

@NgModule({
  declarations: [
    AppComponent,
    ExampleComponent,
    ContentComponent
  ],
  imports: [
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FlexLayoutModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    FormsModule,
    MatRadioModule,
    MatCardModule,
    MatSidenavModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatToolbarModule,
    MatIconModule,
    MatExpansionModule,
    
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
